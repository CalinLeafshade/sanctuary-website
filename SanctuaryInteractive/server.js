﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');

var app = express();

if (app.get('env') === 'development') {
    mongoose.connect('mongodb://192.168.0.10:27017/sanctuary');
}
else {
    mongoose.connect('mongodb://localhost/sanctuary');
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: "pautaunfg"}));
app.use(passport.initialize());
app.use(passport.session());

app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

///load models

fs.readdirSync('./models').forEach(function (file) {
    if (file.substr(-3) === '.js') {
        require('./models/' + file);
    }
});

/// load controllers

fs.readdirSync('./controllers').forEach(function (file) {
    if (file.substr(-3) === '.js') {
        require('./controllers/' + file)(app);
    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

function errorName(code) {
    if (code == 401) return "Unauthorised"
    else if (code == 404) return "Not Found"
    else if (code == 500) return "Bad Request"
    else return ""
}

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            code: err.status + " - " + errorName(err.status),
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        code: err.status
    });
});

app.set('port', process.env.PORT || 3000);
console.log(app.get('port'));
var server = app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + server.address().port);
});


