﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var userSchema = new Schema({
    name: String,
    email: String,
    password: String,
    salt: String
});

userSchema.methods.validPassword = function (password) {
    var shasum = crypto.createHash('sha256');
    shasum.update(password + this.salt);
    return shasum.digest('hex') === this.password;
}

userSchema.methods.setPassword = function (password) {
    var shasum = crypto.createHash('sha256');
    this.salt = crypto.randomBytes(8).toString('hex');
    shasum.update(password + this.salt);
    this.password = shasum.digest('hex');
}

User = mongoose.model('User', userSchema);



