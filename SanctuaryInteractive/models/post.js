﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var marked = require('marked');
var moment = require('moment');

marked.setOptions({
    highlight: function (code, lang) {
        return require('highlight.js').highlight(lang, code).value;
    }
});

var postSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    title: String,
    body: String,
    dateCreated: { type: Date, default: Date.now },
    dateModified: { type: Date, default: Date.now },
    published: Boolean,
    tags: [String],
    slug: String
});

postSchema.methods.generateSlug = function () {
    this.slug = this.title.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
}

postSchema.statics.findBySlug = function (slug, cb) {
    Post.findOne().where({ slug: slug }).exec(function (err, doc) {
        cb(err, doc);
    });
}

postSchema.statics.viewModel = function (p) {
    if (!p)
        return null;
    return {
        user: p.user,
        title: p.title,
        body: marked(p.body),
        dateCreated: moment(p.dateCreated).format('MMMM Do YYYY, h:mm:ss a'),
        slug: p.slug,
        tags: p.tags,
        published: p.published
    };
}

Post = mongoose.model('Post', postSchema);
