﻿
module.exports = function (app) {
    
    var passport = require('passport');
    var LocalStrategy = require('passport-local').Strategy;
    
    passport.use(new LocalStrategy(
        function (email, password, done) {
            User.findOne({ email: email }, function (err, user) {
                if (err) { return done(err); }
                if (!user) {
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (!user.validPassword(password)) {
                    return done(null, false, { message: 'Incorrect password.' });
                }
                return done(null, user);
            });
        }
    ));
    
    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });
    
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });
    
    var ensureAdmin = function (req, res, next) {
        if (!req.user) {
            res.redirect('/login');
        }
        else {
            next();
        }
    }
    
    app.get('/login', function (req, res) {
        res.render('admin/login');
    });

    app.post('/login', passport.authenticate('local', {
        successRedirect: '/admin',
        failureRedirect: '/login'
    }));

    app.get('/admin', ensureAdmin, function (req, res) {
        res.render('admin/admin')
    });

    app.get('/admin/post/create', ensureAdmin, function (req, res) {
        res.render('admin/post/create');
    });
    
    app.get('/admin/post/edit/:slug', ensureAdmin, function (req, res) {
        Post.findBySlug(req.params.slug, function (err, post) {
            if (err) {
                throw new Error(err);
            }
            else if (!post) {
                res.send(404);
            }
            else {
                res.render('admin/post/edit', { post: post });
            }
        });
    });
    
    app.post('/admin/post/edit/:slug', ensureAdmin, function (req, res) {
        Post.findById(req.body._id, function (err, doc) {
            doc.title = req.body.title;
            doc.body = req.body.body;
            doc.tags = req.body.tags.split(", ");
            doc.published = req.body.published
            doc.generateSlug();
            doc.save(function (err) {
                if (err) {
                    es.render('admin/post/edit', { alerts: [{ type: 'danger', message: err }] });
                }
                else {
                    res.redirect('/admin/posts');
                }
            });
        });
    });

    app.post('/admin/post/create', ensureAdmin, function (req, res) {
        
        var post = new Post( {
            title: req.body.title,
            body: req.body.body,
            tags: req.body.tags.split(", "),
            user: req.user._id,
            published: req.body.published
        });

        post.generateSlug();

        post.save(function (err) {
            if (err) {
                res.render('admin/post/create', { alerts: [{ type: 'danger', message: err }] });
            }
            else {
                res.redirect('/admin/posts');
            }
        });
    });

    app.get('/admin/posts', ensureAdmin, function (req, res) {
        
        Post.find().populate('user').sort('dateCreated').exec(function (err, posts) {
            res.render('admin/post/list', { posts: posts });
        });

    });

    app.get('/admin/post/delete/:slug', ensureAdmin, function (req, res) {
        
        Post.findBySlug(req.params.slug, function (err, post) {
            res.render('admin/post/delete', { post: post });
        });

    });

    app.post('/admin/post/delete/:slug', ensureAdmin, function (req, res) {
        
        Post.find({ slug: req.params.slug }).remove().exec(function (err) {
            res.redirect('/admin/posts');
        });
           
    });

}
