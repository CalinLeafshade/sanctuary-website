﻿module.exports = function (app) {
    
    app.get('/blog', function (req, res) {
        res.redirect('/blog/page/1');
    });
    
    app.get('/blog/post/:slug', function (req, res) {
        Post.findOne({ slug: req.params.slug }).populate('user').exec(function (err, doc) {
            if (!doc || (!doc.published && !req.user)) {
                res.send(404);
            }
            else {
                var model = {
                    posts: [Post.viewModel(doc)]
                }
                res.render('blog', model);
            }
        });
    });
    
    app.get('/blog/tag/:tag', function (req, res) {
        
        Post
        .find()
        .populate('user')
        .where({ tags: req.params.tag })
        .sort('-dateCreated')
        .exec(function (err, docs) {
            if (docs && docs.length > 0) {
                res.render('blog', { posts: docs.map(Post.viewModel) });
            }
            else {
                res.send(404);
            }
        });

    });
    
    app.get('/blog/page/:page', function (req, res) {
        
        var postsPerPage = 10;
        
        Post
        .find()
        .populate('user')
        .where({ published: true })
        .sort('-dateCreated')
        .skip(postsPerPage * (req.params.page - 1))
        .limit(postsPerPage)
        .exec(function (err, docs) {
            if (docs && docs.length > 0) {
                res.render('blog', { posts: docs.map(Post.viewModel) });
            }
            else {
                res.send(404);
            }
        });


    });
}