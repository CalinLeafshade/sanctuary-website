﻿module.exports = function (app) {
    
    var RSS = require('rss');

    app.get("/blog/rss", function (req, res) {
        
        /* lets create an rss feed */
        var feed = new RSS({
            title: 'Sanctuary Interactive Blog',
            description: 'The developer blog of Sanctuary Interactive',
            feed_url: 'http://www.sanctuary-interactive.com/feed',
            site_url: 'http://www.sanctuary-interactive.com',
            image_url: 'http://www.sanctuary-interactive.com/img/keyhole.png',
            managingEditor: 'Steven Poulton',
            webMaster: 'Steven Poulton',
            copyright: '2015 Sanctuary Interactive',
            language: 'en',
            categories: ['Gaming', 'Games Development', 'Programming'],
            pubDate: Date.now().toString(),
            ttl: '60'
        });
        
        /* loop over data and add to feed */

        Post.find()
        .populate('user')
        .where({ published: true })
        .sort('-dateCreated')
        .limit(20)
        .exec(function (err, docs) {
            docs.forEach(function (post) {
                feed.item({
                    title: post.title,
                    description: post.body.split('\n').slice(0, 1)[0],
                    url: 'http://www.sanctuary-interactive.com/blog/post/' + post.slug,
                    categories: post.tags,
                    author: post.user.name,
                    date: post.dateCreated
                });
            });
            res.send(feed.xml());
        });

    });

}