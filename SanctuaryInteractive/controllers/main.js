
var fs = require('fs');
var marked = require('marked');

module.exports = function (app) {
    app.get("/", function (req, res) {
        fs.readFile("content/about.md", function (err, data) {
            Post.findOne({ published: true, tags: 'news' }).populate('user').sort('-dateCreated').exec(function (err, doc) {
                var model = {
                    about: marked(String(data)),
                    latestPost: Post.viewModel(doc),
                    games: [
                        {
                            gameID: 1,
                            title: "All The Way Down",
                            description: marked("Winner of the Monthly AGS competition for December 2014, All The Way Down is short, horror, point-and-click adventure game set in rural Yorkshire. \n\n You play as an American hiker lost in a freak storm who happens upon sleepy Millvale in the heart of the Yorkshire Dales.\n\nWill you get out alive?"),
                            thumbnail: "vid/atwd.mp4",
                            link: 'http://gamejolt.com/games/adventure/all-the-way-down/43678/',
                            press: [
                                {
                                    source: "Rock Paper Shotgun",
                                    quote: "Just allow the deceptively pleasant backgrounds to pull you into the darkness of it all and the excellent pixel-art graphics to do their job by letting your imagination fill in all the gaps and you’ll deeply appreciate just how wisely designed All The Way Down really is"
                                },
                                {
                                    source: "Indie Game Mag",
                                    quote: "It may just leave you thinking that the folks at Sanctuary are going to be titans in the point-and-click genre."
                                },
                                {
                                    source: "PC Gamer",
                                    quote: "Cracking spritework, solid writing and strong voice acting bring the short-ish horror to life."
                                }
                            ]
                        },
                        {
                            gameID: 2,
                            title: "Latency",
                            description: marked("Latency is an upcoming, cyberpunk adventure game from Sanctuary Interactive. \n\n Kass_Seti, a leading member of hacktivist group Analogue Dawn, wakes on the streets of New Soho with a hell of a headache. Her advanced cybernetic implants have malfunctioned, corrupting her memory, leaving her alone and offline on the streets of a London in crisis."),
                            thumbnail: "vid/latency.mp4",
                            website: "http://www.latencygame.com",
                            press: []
                        }
                    ],
                    members: [
                        {
                            name: "Rebecca McCarthy",
                            twitter: "azuresama",
                            website: "http://shonen.co.uk/",
                            roles: ["Writer", "Designer", "Director", "Voice Actor"],
                            bio: "Rebecca is an accomplished writer whose work can be found printed in several acclaimed mangas and performed as audioplays. She also writes regularly for Otaku News under the pseudonym Azure. When not writing she can be found refining her skills as a voice over artist.\n\nIn Sanctuary, Rebecca acts as the driving force behind our productions."
                        },
                        {
                            name: "Matt Frith",
                            twitter: "MatthewJFrith",
                            website: "http://www.mattfrith.com",
                            roles: ["Sprite Artist"],
                            bio: "Matt is a current rising star in the pixel art scene. His work can be found in hit Mobile game Katatak and all over the internet. Matt is also a successful freelance illustrator whose work is as at home in print as it is on screen. When not pixelling Matt can be found improving his social media presence and playing with trains."
                        },
                        {
                            name: "Hayley Griffiths",
                            twitter: "SookieSock",
                            website: "http://shadowedbygiants.tumblr.com/",
                            roles: ["Artist", "Illustrator"],
                            bio: "Hayley is an experienced digital artist with a traditional Fine Art background. She is also a lecturer in Game Design and Art at Hugh Baird College in Liverpool. In the rare occasions you might not find Hayley drawing, she will probably be terrorising her cats."
                        },
                        {
                            name: "Steven Poulton",
                            twitter: "CalinLeafshade",
                            website: "http://www.leafsha.de",
                            roles: ["Programmer", "Writer"],
                            bio: "As a web developer by profession, Steve is an experienced programmer. He is also responsible for the games Eternally Us and The McCarthy Chronicles"
                        },
                        {
                            name: "Drew Wellman",
                            twitter: "ddq5",
                            website: "http://automaticity.tumblr.com/",
                            roles: ["Programmer", "Writer", "Voice Actor"],
                            bio: "Drew"
                        }
                    ]
                };
                res.render("main", model);
            });
        });
    });

}