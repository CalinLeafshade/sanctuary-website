
function checkNav () {

	var scrollTop = $(window).scrollTop();

	if (scrollTop > $('.brand-header').height() - 10) {
		$('.navbar').removeClass('hideNav');
	}
	else {
		$('.navbar').addClass('hideNav');
	}

	var offsets = [];

	$('ul.nav.navbar-nav li').removeClass('active');

	$('.js-scroll-link').each(function (i,e) {
		var t = $(e).attr('href');
		var $target = $(t);
		if ($target.length) {
			offsets.push({
				element: e,
				top: $target.offset().top
			});
		}
	});

	offsets = offsets.sort(function (a,b) {
		if (a.top < b.top)
			return -1;
		else if (a.top > b.top) 
			return 1;
		else
			return 0;
	});

	for (var i = 0; i < offsets.length; i++) {
		var o = offsets[i];
		var next = offsets[i + 1];
		if (next) {
			if (scrollTop + 51 < next.top)
			{
				$(o.element).parent().addClass('active');
				break;
			}
		}
		else {
			$(o.element).parent().addClass('active');
		}
	};

}

$(function() {

    checkNav();
    
    $('code').parent('pre').addClass('hljs');
    
    if ($('footer').prev().hasClass("light")) {
        $('footer').addClass('dark');
    }

	$(window).on('scroll', function (ev) {
		checkNav();
	});

	$('.js-scroll-link').on('click', function (ev) {
		var $this = $(this);
		var t = $this.attr('href');
		ev.preventDefault();
		var $target = $(t);
		if ($target.length) {
			$('html,body').animate({
				scrollTop: ($target.offset().top - 50) + "px"
			});
			checkNav();
		}
	});
})